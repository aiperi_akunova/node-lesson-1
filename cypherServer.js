const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app =express();
const port = 8000;

const password = 'password';

app.get('/encode/:text', (req,res)=>{
    const answer = Vigenere.Cipher(password).crypt(req.params.text);
    res.set({'Content-type': 'text/plain'}).send(answer);
});

app.get('/decode/:text', (req,res)=>{
    const answer = Vigenere.Decipher(password).crypt(req.params.text);
    res.set({'Content-type': 'text/plain'}).send(answer);
});

app.get('/', (req,res)=>{
    res.set({'Content-type': 'text/plain'}).send('Cypher server');
});

app.listen(port, ()=>{
    console.log('We are on port '+port)
});