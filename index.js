const express = require('express');

const app =express();
const port = 8000;

app.get('/:something', (req,res)=>{
    res.set({'Content-type': 'text/plain'}).send(req.params.something);
})

app.get('/', (req,res)=>{
    res.set({'Content-type': 'text/plain'}).send('Home');
})

app.listen(port, ()=>{
    console.log('We are on port '+port)
});